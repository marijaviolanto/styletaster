<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog-content');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|g^TF+nVuf)tJDxVa-V|E`s= ~R5]Eme,P|I|`RVC^O%}x&mX1ipxB=Vno0)K!1c');
define('SECURE_AUTH_KEY',  'M8>Ka sYJs3pePS^y|l%y:RNz}<:. UYJ<~9esIpW(Y0<q;Vc/FzBfy,GG/)uz6N');
define('LOGGED_IN_KEY',    '.@e]qlzv`Tj9 H[&I[a4oa6XNh|,-!.vD!gLvB]qCuX$+&BX]_WDnwg*_s`lb;]w');
define('NONCE_KEY',        ';cC3R+,SL(oY%dA#,I_S[^z%N7%SHT*djS~=8$[qP<FfD]c`[8~7pHN&9r]+#/s)');
define('AUTH_SALT',        'qR*AMR-]~Pt{cAIAp/*a~.W{aZHkk415$6/H4L]tNG~;$WX~: +I3mS}2-[Hj[Xt');
define('SECURE_AUTH_SALT', '8,0x]lW^P(}@R,Uf!uS2ODcm@!&IxI=S:x}d_qUL+WAD1Q.O.ms-u)Nn9x2Y@_6|');
define('LOGGED_IN_SALT',   '/IkWrzbuEp0<h+UB[hcV|ySI/S5(4mScStE-Zz%]])vy,$ag*jfOp} @F+D/L.@>');
define('NONCE_SALT',       '@2&kBzq,2:*$8mB<0{!9@5GuP;D4j#>jLL6:E&l.g,CbMgIyUY*8X.i=Vy~Tvu-6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Sets up 'direct' method for wordpress, auto update without ftp */
define('FS_METHOD','direct');
