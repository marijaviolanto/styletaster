<!-- Begin Footer -->
	<footer id="footer" class="clearfix">
		<div class="content dark">

		</div>

		<div class="container">

			

			<!-- Copyright Info -->
			<div class="copyright grid-full"><h6>©<?php echo date('Y'); ?> <a href="<?php echo esc_url(home_url( '/' )); ?>"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-footer.png" width="91" height="30" alt="styleTester"></a>. All rights reserved.</h6></div>

		</div>
	</footer>
	<!-- End Footer -->



<?php wp_footer(); ?>


</body>


</html>

